Test how to
- Look up docker network name in the template files in the ansible directory

- Run `sudo docker network inspect <docker network name> | grep Gateway`
which shoulld give something like:
`"Gateway": "172.18.0.1"`

- Use the given Gateway-adress and run `ip addr | grep <Gateway-adress>`
Output shuold be something like:
`inet 172.18.0.1/16 brd 172.18.255.255 scope global br-90a6620f3525`

- The last entry in the output will be your networkinterface for your docker network. In this examples case it's `br-90a6620f3525`.

- Start Wireshark as sudo or a user in the wireshark group depending on your setup, sudo always works if you're unsure and point it at the interface we found.

- You will now get all data from your docker network. You can use the filter `tcp and mqtt` to only show mqtt messages as long as these use TCP which should be standard.